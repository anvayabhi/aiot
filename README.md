## Project Template


Farm stray animal alert
 
Send WhatsApp alert and a ring buzzer, if stray animal is detected on farm.


## Shunya Stack
Project is built by using the Shunya stack.

-   ShunyaOS is a lightweight Operating system with built-in support for AI and IOT.
-   Shunya Stack Low Code platform to build AI, IoT and AIoT products, that allows you to rapidly create and deploy AIoT products with ease.

For more information on ShunyaOS see : http://demo.shunyaos.org


## Documentation
For developers see detailed Documentation on the components of the project in the [Wiki](https://gitlab.com/iotiotdotin/community-projects/dev-exp-template/-/wikis/home)

## Project Overview (Pending update later)

1.  [Project Plan Excel]()
1.  [Get a birds-eye status of the project](https://chanchal-kumawat20.medium.com/quality-resume-in-just-two-days-a6f7d928897e )


## Contributing
Help us improve the project.

Ways you can help:

1.  Choose from the existing issue and work on those issues.
2.  Feel like the project could use a new feature, make an issue to discuss how it can be implemented and work on it.
3.  Find a bug create an Issue and report it.
4.  Review Issues or Merge Requests, give the developers the feedback.
5.  Fix Documentation.

## Contributors 

#### Team Lead 
1. [Anvay Deshpande](anvayabhi@gmail.com)

#### Active Contributors.

1.  Member1 - [Chanchal Kumawat](chanchal.kumawat20@vit.edu)
2.  Member2 - [Shrikant Bhadgaonkar](shrikantbhadgaonkar@gmail.com)

````

